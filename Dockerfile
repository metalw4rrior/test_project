FROM python:3.9-alpine

WORKDIR /day3/

COPY ./programm.py .
COPY ./README.md .
COPY ./script.sh .

CMD ["sh", "script.sh"]

